﻿var table;

$(document).ready(function() {
    table = crudNamespace.loadEmployeeTable();
    $(document).ajaxError(function (event, request, settings) {
        crudNamespace.showModal("An error occurred: " + request.status + " " + request.statusText + " URL: " + settings.url);
    });

    }

);

var crudNamespace = {
    loadEmployeeTable: function() {
        return $('#emplTable').DataTable({
            processing: true,
            serverSide: true,
            "ajax": {
                url: "/Employees/LoadData",
                type: 'POST',
            },
            "columns": [
                { "data": "EmployeeId", "autoWidth": true, "visible": false },
                { "data": "FirstName", "autoWidth": true, },
                { "data": "LastName", "autoWidth": true },
                { "data": "DeptId", "autoWidth": true, "visible": false },
                { "data": "DeptName", "autoWidth": true },
                {
                    "data": "LastModifiedAt",
                    "autoWidth": true,
                    "render": function(data) {
                        return moment(data).format('lll');
                    }
                },
                {
                    "data": null,
                    "className": "center",
                    "render": function(data, type, row) {
                        return '<input type="button" value="Edit" onclick="crudNamespace.showEditForm(' + row.EmployeeId + ')" class="btn btn-basic btn-xs">';
                    },
                    "targets": 0,
                    "orderable": false
                },
                {
                    "data": null,
                    "className": "center",
                    "render": function(data, type, row) {
                        return '<input type="button" value="Delete" onclick="crudNamespace.showDeleteForm(' + row.EmployeeId + ')" class="btn btn-danger btn-xs">';
                    },
                    "targets": 0,
                    "orderable": false
                }
            ]
        });
    },


   


    showEditForm: function(id) {
        $.ajax({
            url: 'Employees/Edit',
            method: "GET",
            data: { id: id },
            success: crudNamespace.onEditFormReceived,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    editEmployee: function() {
        $.ajax({
            url: 'Employees/Edit',
            method: "POST",
            data: $("#EditEmployeeForm").serialize(),
            success: crudNamespace.onEditFormHide,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    onEditFormReceived: function(formContent) {
        $('#DeleteEmployeeDiv').hide();
        $('#CreateEmployeeDiv').hide();
        $('#EditEmployeeDiv').html(formContent);
        $('#EditEmployeeDiv').slideDown(500);
        $("#EditEmployeeForm").validate();
        //var form = $('#EditEmployeeForm');
        //$.validator.setDefaults({ ignore: null });
        //$.validator.unobtrusive.parse(form);
    },

    onEditFormHide: function() {
        $.validator.setDefaults({ ignore: '' });
        // $("#FirstName").valid();
        $("#EditEmployeeForm").validate();
        $('#EditEmployeeDiv').hide(500);
        table.ajax.reload();
    },

    showDeleteForm: function(id) {
        $.ajax({
            url: 'Employees/Delete',
            method: "GET",
            data: { id: id },
            success: crudNamespace.onDeleteFormReceived,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    deleteEmployee: function() {
        var id = $('#EmployeeId').val();
        $.ajax({
            url: 'Employees/DeleteConfirmed',
            method: "GET",
            data: { id: id },
            success: crudNamespace.onDeleteFormHide,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    onDeleteFormReceived: function(formContent) {
        $('#EditEmployeeDiv').hide();
        $('#CreateEmployeeDiv').hide();
        $('#DeleteEmployeeDiv').html(formContent);
        $('#DeleteEmployeeDiv').slideDown(500);
    },

    onDeleteFormHide: function() {
        $('#DeleteEmployeeDiv').hide(500);
        table.ajax.reload();
    },

    showCreateForm: function() {
        $.ajax({
            url: 'Employees/Create',
            method: "GET",
            success: crudNamespace.onCreateFormReceived,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    createEmployee: function() {
        $.ajax({
            url: 'Employees/Create',
            method: "POST",
            data: $("#CreateEmployeeForm").serialize(),
            success: crudNamespace.onCreateFormHide,
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    },

    onCreateFormReceived: function(formContent) {
        $('#DeleteEmployeeDiv').hide();
        $('#EditEmployeeDiv').hide();
        $('#CreateEmployeeDiv').html(formContent);
        $('#CreateEmployeeDiv').slideDown(500);
    },

    onCreateFormHide: function() {
        $('#CreateEmployeeDiv').hide(500);
        table.ajax.reload();
    },

    onFormSubmit: function() {
        $.validator.unobtrusive.parse(document);
    },


    showModal: function (messageText) {
        $('.modal-body').text(messageText);
        $("#myModal").modal("show");
    },

    editDepartment: function() {
        $('#departmentTable').DataTable({
            "ajax": {
                "url": "/Department/LoadData",
                "type": "GET",
                "datatype": "json",
            },
            "columns": [
                { "data": "DepartmentId", "autoWidth": true },
                { "data": "Name", "autoWidth": true, },
                {
                    "data": null,
                    "className": "center",
                    "render": function(data, type, row) {
                        return '<a href="#" onclick="showDepartmentDetails(' + row.DepartmentId + ')">Edit</a>';
                    }
                },
                {
                    "data": null,
                    "className": "center",
                    "render": function(data, type, row) {
                        return '<a href="#" onclick="showDepartmentDetails(' + row.DepartmentId + ')">Delete</a>';
                    }
                }
            ]
        });
    },


    showDepartmentDetails: function(id) {
        $('.edit_class').slideDown();
        $.ajax({
            url: 'Department/Edit',
            method: "GET",
            data: { id: id },
            success: function(result) {
                $('#editForm').html(result);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
};