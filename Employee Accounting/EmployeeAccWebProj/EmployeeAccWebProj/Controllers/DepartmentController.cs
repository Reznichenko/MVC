﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using BusinessLogic;
using BusinessLogic.ViewModel;

namespace EmployeeAccWebProj.Controllers
{
    public class DepartmentController : Controller
    {
        public ActionResult Index()
        {
            using (var departmentService = new DepartmentService())
            {
                var department = departmentService.GetData();
                return View(department);
            }
        }

        public ActionResult LoadData()
        {
            using (var departmentService = new DepartmentService())
            {
                var department = departmentService.GetData();
                var data = department.OrderBy(a => a.Name);
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var departmentService = new DepartmentService())
            {
                var data = departmentService.GetData();
                var department = data.Find(a => a.DepartmentId == id);
                if (department == null)
                {
                    return HttpNotFound();
                }
                return View(department);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DepartmentId,Name")] DepartmentViewModel department)
        {
            using (var departmentService = new DepartmentService())
            {
                if (ModelState.IsValid)
                {

                    departmentService.Modify(department);
                    return RedirectToAction("Index");
                }

                return View(department);
            }
        }
    }
}