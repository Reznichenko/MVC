﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BusinessLogic;
using BusinessLogic.Entity;
using BusinessLogic.Paging;
using BusinessLogic.ViewModel;

namespace EmployeeAccWebProj.Controllers
{
    public class EmployeesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Returns Json data with Employees 
        /// </summary>
        /// <param name="model">Object that represents datatable paging settings</param>
        /// <returns></returns>
        public JsonResult LoadData(DataTableAjaxPostModel model)
        {
            if (model.search == null)
            {
                model.search = new Search { value = "" };
            }
            int totalRecords;
            int recordsFiltered;

            var employees = new EmployeeRepository().GetPaginated(model, out totalRecords, out recordsFiltered);
            return Json(new
            {
                draw = model.draw,
                recordsTotal = totalRecords,
                recordsFiltered = recordsFiltered,
                data = employees

            }, JsonRequestBehavior.AllowGet);
        }

        private static IEnumerable<DepartmentEntity> DepartmentsGetData()
        {
            List<DepartmentEntity> department;
            using (var departmentService = new DepartmentService())
            {
                department = departmentService.GetData();
            }
            return department;
        }
        /// <summary>
        /// Returns empty Create form using Get method
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            PopulateDepartmentsDropDownList();
            return PartialView();
        }

        /// <summary>
        /// Saves new employee to database using Post method
        /// </summary>
        /// <param name="employee">Employee view model object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirstName,LastName,DeptId,DeptName")] EmployeeViewModel employee)
        {

            using (var employeeService = new EmployeeService())
            {
                if (!ModelState.IsValid)
                {
                    PopulateDepartmentsDropDownList();
                    return HttpNotFound();
                }
                employeeService.Create(employee);
                return Json("");
            }
        }

        /// <summary>
        /// Returns Edit form with employee data using Get method
        /// </summary>
        /// <param name="id">Int id of record to edit</param>
        /// <returns></returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var departments = DepartmentsGetData();
            List<EmployeeViewModel> data;
            using (var employeeService = new EmployeeService())
            {
                var employees = employeeService.GetData();
                data = employeeService.LoadEntity(employees, departments);
            }

            var employee = data.Find(a => a.EmployeeId == id);

            if (employee == null)
            {
                return HttpNotFound();
            }
            PopulateDepartmentsDropDownList(employee.EmployeeId);
            return PartialView(employee);
        }

        /// <summary>
        ///  Saves edited employee to database using Post method
        /// </summary>
        /// <param name="employee">Employee view model object</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmployeeId,FirstName,LastName,DeptId,DeptName,LastModifiedAt")] EmployeeViewModel employee)
        {
            using (var employeeService = new EmployeeService())
            {
                if (ModelState.IsValid)
                {

                    employeeService.Edit(employee);
                    return RedirectToAction("Index");
                }

                PopulateDepartmentsDropDownList(employee.EmployeeId);
                return PartialView(employee);
            }
        }

        /// <summary>
        /// Returns Delete form with employee data using Get method
        /// </summary>
        /// <param name="id">Int id of record to delete</param>
        /// <returns></returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var departments = DepartmentsGetData();
            using (var employeeService = new EmployeeService())
            {
                var employees = employeeService.GetData();
                var data = employeeService.LoadEntity(employees, departments);
                var employee = data.First(x => x.EmployeeId == id);
                if (employee == null)
                {
                    return HttpNotFound();
                }
                return PartialView(employee);
            }
        }

        /// <summary>
        /// Deletes record from database by id
        /// </summary>
        /// <param name="id">Int id of record to delete</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteConfirmed(int id)
        {
            using (var employeeService = new EmployeeService())
            {
                employeeService.Delete(id);
                return RedirectToAction("Index");
            }
        }

        private void PopulateDepartmentsDropDownList(object selectedDepartment = null)
        {
            var data = DepartmentsGetData();
            ViewBag.DeptID = new SelectList(data, "DepartmentID", "Name", selectedDepartment);
        }
    }
}
