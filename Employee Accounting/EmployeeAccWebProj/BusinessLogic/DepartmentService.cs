﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Entity;
using BusinessLogic.ViewModel;
using DataAccessLayer;

namespace BusinessLogic
{
    public class DepartmentService : IDisposable
    {
        readonly Context _context = new Context();
        /// <summary>
        /// Returns DepartmentEntity List with all records in Department table
        /// </summary>
        public List<DepartmentEntity> GetData()
        {
            var departmentViewModel = _context.Departments.Select(x => new DepartmentEntity()
            {
                DepartmentId = x.DepartmentId,
                Name = x.Name,
            }).ToList();
            return departmentViewModel;
        }
        /// <summary>
        /// Calls the Dispose method
        /// </summary>
        public void  Dispose()
        {
            _context.Dispose();
        }
        /// <summary>
        /// Calls SaveChanges method
        /// </summary>
        /// <param name="departmentviewModel"></param>
        public void Modify(DepartmentViewModel departmentviewModel)
        {
            SaveChanges();
        }
        /// <summary>
        /// Saves changes to the database
        /// </summary>
        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
