﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.ViewModel
{
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }
        [Display(Name = "First Name")]
        [Required(AllowEmptyStrings = false)]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }
        [Display(Name = "Department Id")]
        public int DeptId { get; set; }
        [Display(Name = "Department Name")]
        public string DeptName { get; set; }
        public DateTime LastModifiedAt { get; set; }
    }
}
