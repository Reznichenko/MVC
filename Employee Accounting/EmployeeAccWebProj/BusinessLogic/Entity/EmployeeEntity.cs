﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Entity
{
   public class EmployeeEntity
    {
        public int EmployeeId { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Department")]
        public int DeptId { get; set; }
        public DateTime LastModifiedAt { get; set; }
    }
}
