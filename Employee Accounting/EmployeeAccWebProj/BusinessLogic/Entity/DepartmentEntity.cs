﻿namespace BusinessLogic.Entity
{
    public class DepartmentEntity
    {
        public int DepartmentId { get; set; }
        public string Name { get; set; }
    }
}
