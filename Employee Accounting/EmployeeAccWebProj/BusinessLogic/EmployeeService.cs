﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Entity;
using BusinessLogic.ViewModel;
using DataAccessLayer;

namespace BusinessLogic
{
    public class EmployeeService : IDisposable
    {
        private readonly Context _context = new Context();

        /// <summary>
        /// Returns EmployeeEntity List with all records in Employee table
        /// </summary>
        public List<EmployeeEntity> GetData()
        {
            var employeeViewModel = _context.Employees.Select(x => new EmployeeEntity
            {
                DeptId = x.DeptId,
                EmployeeId = x.EmployeeId,
                FirstName = x.FirstName,
                LastModifiedAt = x.LastModifiedAt,
                LastName = x.LastName
            }).ToList();
            return employeeViewModel;
        }
        /// <summary>
        /// Returns EmployeeViewModel List with Department Names 
        /// </summary>
        /// <param name="emloyeViewModels"></param>
        /// <param name="departmentViewModels"></param>
        /// <returns></returns>
        public List<EmployeeViewModel> LoadEntity(IEnumerable<EmployeeEntity> emloyeViewModels, IEnumerable<DepartmentEntity> departmentViewModels)
        {
            return emloyeViewModels.Select(x => new EmployeeViewModel
            {
                EmployeeId = x.EmployeeId,
                FirstName = x.FirstName,
                LastName = x.LastName,
                DeptId = x.DeptId,
                DeptName = departmentViewModels.First(d => d.DepartmentId == x.DeptId).Name,
                LastModifiedAt = x.LastModifiedAt
            }).ToList();
        }
        /// <summary>
        /// Calls the Dispose method
        /// </summary>
        public void Dispose()
        {
            _context.Dispose();
        }

        /// <summary>
        /// Saves edited model to the database
        /// </summary>
        /// <param name="employeeViewModel"></param>
        public void Edit(EmployeeViewModel employeeViewModel)
        {
            var employeeDataModel = _context.Employees.First(x => x.EmployeeId == employeeViewModel.EmployeeId);
            employeeDataModel.EmployeeId = employeeViewModel.EmployeeId;
            employeeDataModel.FirstName = employeeViewModel.FirstName;
            employeeDataModel.LastName = employeeViewModel.LastName;
            employeeDataModel.DeptId = employeeViewModel.DeptId;
            employeeDataModel.LastModifiedAt = DateTime.Now;
            SaveChanges();
        }

        /// <summary>
        /// Deletes record from database by id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            var employeeDataModel = _context.Employees.Find(id);
            _context.Employees.Remove(employeeDataModel);
            SaveChanges();
        }

        private void SaveChanges()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// Creates new record in database using specified model
        /// </summary>
        /// <param name="employeeViewModel"></param>
        public void Create(EmployeeViewModel employeeViewModel)
        {
            _context.Employees.Add(
                new Employee
                {
                    FirstName = employeeViewModel.FirstName,
                    LastName = employeeViewModel.LastName,
                    Department = _context.Departments.First(d => d.DepartmentId == employeeViewModel.DeptId),
                    LastModifiedAt = DateTime.Now
                });
            SaveChanges();
        }
    }
}
