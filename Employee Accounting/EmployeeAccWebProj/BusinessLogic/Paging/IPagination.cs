﻿using System.Linq;

namespace BusinessLogic.Paging
{
    public interface IPagination<T> where T:class
    {
        IQueryable<T> GetPaginated(DataTableAjaxPostModel model, out int totalRecords, out int recordsFiltered);
    }
}
