﻿using System;
using System.Linq;
using BusinessLogic.Extentions;
using BusinessLogic.Helper;
using DataAccessLayer;
using BusinessLogic.ViewModel;

namespace BusinessLogic.Paging
{
    public class EmployeeRepository : IPagination<EmployeeViewModel>
    {
        private readonly Context _context = new Context();

        /// <summary>
        /// Returns filtered and sorted records based on datatables callback
        /// </summary>
        /// <param name="model"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<EmployeeViewModel> GetPaginated(DataTableAjaxPostModel model, out int totalRecords, out int recordsFiltered)
        {
            var data = _context.Employees.AsQueryable();
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(model.search.value))
            {
                data = Filter(model.search.value, data);
            }

            var query = (from employee in data
                join dept in _context.Departments on employee.DeptId equals dept.DepartmentId
                select new EmployeeViewModel
                {
                    EmployeeId = employee.EmployeeId,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    DeptId = employee.DeptId,
                    DeptName = dept.Name,
                    LastModifiedAt = employee.LastModifiedAt

                }
                );         

            recordsFiltered = data.Count();
            bool isAscending;
            var sortBy = Order(model, out isAscending);

            var result = query.Order(sortBy, isAscending).Skip((model.start/10 * model.length)).Take(model.length);
            return result;
        }

        private static IQueryable<Employee> Filter(string searchValue, IQueryable<Employee> data)
        {
            return data.Where(x => x.FirstName.Contains(searchValue) 
                || x.LastName.Contains(searchValue) 
                || x.Department.Name.Contains(searchValue));
        }

        private static string Order(DataTableAjaxPostModel model, out bool isAscending)
        {
            var sortBy = "";
            isAscending = true;
            foreach (var column in model.order)
            {
                sortBy = model.columns[Convert.ToInt32(column.column)].data;
                isAscending = column.dir == Direction.Asc;
            }
            return sortBy;
        }
    }
}
