﻿namespace BusinessLogic.Paging
{
    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }
}
