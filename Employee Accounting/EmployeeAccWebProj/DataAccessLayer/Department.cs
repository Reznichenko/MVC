﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer
{
    public class Department
    {
        public Department()
        {
            Employees = new List<Employee>();
        }

        [Key]
        public int DepartmentId { get; set; }
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
