﻿using System.Data.Entity;

namespace DataAccessLayer
{
    public class Context : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Department> Departments { get; set; }
    }
}
