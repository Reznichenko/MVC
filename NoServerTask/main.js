
$(document).ready(function() {

  buildEmployeesTable(employees);
  var table = $('#emplTable').DataTable();
  
  appendDepartmentSelector(departments);

  $('#emplTable').on('click', 'button', function() {
    var id = findClosestId(this); 
    fillInputs(id);        
  });

  $('#editFrm').on('click', '#submit', function() {
    var id = $('#id').val();
    newData = buildNewData(id);
    table.row(id - 1).data(newData).draw();
    $('.edit_class').hide();
  });
});


function buildNewData(id) {
    var fname = $('#firstname').val();
    var lname = $('#lastname').val();
    var deptid = $('#depNameSelector').val();
    var deptname = getJsonValueById(departments,deptid)[0].Name;
    var lastmodif = getDate();
    var editBtn = $('#editButton').clone()[0].outerHTML;
    return [id,fname,lname,deptname, lastmodif, editBtn];
}

function getDate() {
    var d = new Date();
    var date = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
    var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
    return  date + "T" + time;
}

function findClosestId(table) {
    return $(table).closest('tr').children('td.id').text();
}

function appendDepartmentSelector(departments) {
    $.each(departments, function(i, option) {
        $('#depNameSelector').append($('<option/>').attr("value", option.Id).text(option.Name));
   });
}

function fillInputs(id)
{
    var returnedData = getJsonValueById(employees,id);

    $('.edit_class').slideDown();
    $('#id').val(returnedData[0].Id);
    $('#firstname').val(returnedData[0].FName);
    $('#lastname').val(returnedData[0].LName);
    $('#depNameSelector').val(returnedData[0].DeptId);
}

function getJsonValueById(json,id)
{
     return $.grep(json, function (element, index) {
     return element.Id == id;
     });
}

function buildEmployeesTable(employees) {
     var button = $('#editButton').removeClass();
    $.each(employees, function(i, option) {

    $("#emplTable").find('tbody')
    .append($('<tr>')
        .append($('<td>')
            .text(option.Id)
            )
        .append($('<td>')
            .text(option.FName)
            )
        .append($('<td>')
            .text(option.LName)
            )
        .append($('<td>')
            .text(getJsonValueById(departments,option.DeptId)[0].Name)
            )
        .append($('<td>')
            .text(option.LastModifiedAt)
            )
        .append($('<td>')
            .append(button).clone()
            )
        );
    });
    $('#emplTable td:first-child').addClass('id');
}