var employees = [
	{
		FName: 'Santa', Id: 1, LName: 'Claus', DeptId: 1, LastModifiedAt: '2016-12-24T23:59:59.000Z'
	},
	{
		FName: 'Mickey', Id: 2, LName: 'Mouse', DeptId: 1, LastModifiedAt: '2016-12-24T23:59:59.000Z'
	},
	{
		FName: 'Bat', Id: 3, LName: 'Man', DeptId: 2, LastModifiedAt: '2016-12-24T23:59:59.000Z'
	},
	{
		FName: 'Super', Id: 4, LName: 'Man', DeptId: 3, LastModifiedAt: '2016-12-24T23:59:59.000Z'
	},
	{
		FName: 'Grois', Id: 5, LName: 'Man', DeptId: 4, LastModifiedAt: '2016-12-24T23:59:59.000Z'
	},
	{
		FName: 'Bush', Id: 6, LName: 'Man', DeptId: 5, LastModifiedAt: '2016-12-24T23:59:59.000Z'
	},
]
