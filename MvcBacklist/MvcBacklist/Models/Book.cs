﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcBacklist.Models
{
    public class Book
    {
        public int BookId { get; set; }
        
        [Display(Name = "Book Title"), Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Display(Name = "Release Date"), DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? ReleaseDate { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
        
        [Display(Name = "Book Rating", Description = "Enter an integer between 0 and 10."), Range(0, 10)]
        public int? Rating { get; set; }
        
        [Display(Name = "Number of pages in book")]
        public int? NumberOfPages { get; set; }
    }
}