﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;

namespace MvcBacklist.Models
{
    public class DbMapper
    {
        public List<BookViewModel> Map(DbSet<Book> books)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Book, BookViewModel>()
                .ForMember(a => a.Authors, s => s
                    .MapFrom(src => (string.Join(",", src.Authors
                        .Select(y=>y.LastName))))));
            var mapper = config.CreateMapper();
            var bookViewModel = new List<BookViewModel>();
            foreach (var book in books)
            {
               bookViewModel.Add(mapper.Map<BookViewModel>(book));
            }
            return bookViewModel;
        }
    }
}