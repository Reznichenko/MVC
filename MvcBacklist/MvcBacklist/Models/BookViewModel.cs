﻿namespace MvcBacklist.Models
{
    public class BookViewModel
    {
        public string BookId { get; set; }

        public string Title { get; set; }

        public string ReleaseDate { get; set; }

        public string Authors { get; set; }

        public string Rating { get; set; }

        public string NumberOfPages { get; set; }
    }
}
