﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MvcBacklist.Models
{
    public class BackListDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
    }
}