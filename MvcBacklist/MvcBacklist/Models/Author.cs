﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcBacklist.Models
{
    public class Author
    {
        public int AuthorId { get; set; }
        
        [Required, Display(Name = "Name of Author")]
        public string Name { get; set; }
        
        [Display(Name = "Last Name of Author")]
        public string LastName { get; set; }
        
        [Display(Name = "Number of books")]
        public ushort BooksNumber { get; set; }
        
        public virtual ICollection<Book> Books { get; set; }
    }
}