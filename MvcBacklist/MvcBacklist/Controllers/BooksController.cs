﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MvcBacklist.Models;
using MvcBacklist.Models.Parameters;

namespace MvcBacklist.Controllers
{
    public class BooksController : Controller
    {
        private readonly BackListDbContext _db = new BackListDbContext();

        // GET: Books
        public ActionResult Index()
        {
            return View(_db.Books.ToList());
        }
        //public ActionResult LoadData()
        //{
        //    var mapper = new DbMapper();
        //    var data = mapper.Map(_db.Books);
        //    var s =  Json(new {data}, JsonRequestBehavior.AllowGet);
        //    return s;
        //}

        public JsonResult LoadData(DtParameterModel model)
        {
            var mapper = new DbMapper();
            var data = mapper.Map(_db.Books);
            var json = Json(new
            {
                iTotalRecords = model.Length,
                iTotalDisplayRecords = model.Length,
                aaData = data
            },
                JsonRequestBehavior.AllowGet);
            return json;
        }

        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var book = _db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return PartialView(book);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            ViewBag.Authors = new MultiSelectList(_db.Authors, "AuthorId", "Name");
            return View();
        }

        // POST: Books/Create
        [HttpPost]
        public ActionResult Create(Book book, int[] authors)
        {
            if (authors != null)
            {
                foreach (var authorId in authors)
                {
                    var author = _db.Authors.Find(authorId);
                    book.Authors.Add(author);
                }
            }
            _db.Books.Add(book);
            _db.SaveChanges();
            return View("Index", _db.Books);
        }


        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Title,ReleaseDate,Rating,NumberOfPages")] Book book)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Books.Add(book);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(book);
        //}

        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = _db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,ReleaseDate,Rating,NumberOfPages")] Book book)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(book).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(book);
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = _db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = _db.Books.Find(id);
            _db.Books.Remove(book);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
