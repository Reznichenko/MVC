﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using MVCDatatable.Models;

namespace MVCDatatable.Controllers
{
    public class MyController : Controller
    {
        //
        // GET: /My/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData()
        {
            using (var db = new MyDbEntity())
            {
                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                var data = db.Books.OrderBy(a => a.Title).ToList();
                return Json(new {data}, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(int id)
        {
            using (var db = new MyDbEntity())
            {
                db.Configuration.LazyLoadingEnabled = false;
                var model = db.Books.FirstOrDefault(b => b.Id == id) ?? new Book();

                return PartialView(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,ReleaseDate,Rating,NumberOfPages")] Book book)
        {
            using (var db = new MyDbEntity())
            {
                if (!ModelState.IsValid) return View(book);
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return View(book);
            }
        }

        [HttpPost]
        public ActionResult Submit([Bind(Include = "Id,Title,ReleaseDate,Rating,NumberOfPages")] Book book)
        {
             using (var db = new MyDbEntity())
            {
                if (!ModelState.IsValid) return View(book);
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}
