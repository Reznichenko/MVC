﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookList.Controllers
{
    public class BookController : Controller
    {
        //
        // GET: /Book/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData()
        {
            using (var dbContext = new BookDatabaseEntities())
            {
                var data = dbContext.Books.ToList();
                return Json(new { data }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
